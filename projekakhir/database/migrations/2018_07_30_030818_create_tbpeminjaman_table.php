<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbpeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbpeminjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_pinjam'); 
            $table->string('tgl_kembali'); 
            $table->string('id_petugas'); 
            $table->string('id_siswa'); 
            $table->string('id_buku'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbpeminjaman');
    }
}
