<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbbukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbbuku', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_buku'); 
            $table->string('judul'); 
            $table->string('penulis'); 
            $table->string('penerbit');  
            $table->string('thn_terbit');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbbuku');
    }
}
