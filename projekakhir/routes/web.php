<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('masterlayout.content');
// });
Route::get('/','haldepController@index');

Route::get('admin', function (){
	return View('masterlayout.admin');
}); 

Route::get('novel', function (){
	return View('masterlayout.novel');
});
Route::get('biografi', function (){
	return View('masterlayout.biografi');
});
Route::get('komik', function (){
	return View('masterlayout.komik');
});
Route::get('dongeng', function (){
	return View('masterlayout.dongeng');
});

Route::get('pemasukan','BukuController@index');
Route::get('/tambahmasuk','BukuController@create');
Route::post('/tambahmasuk','BukuController@store');
Route::post('/updatemasuk/{id}','BukuController@update');
Route::get('/hapusbuku/{id}','BukuController@destroy');
Route::get('/editmasuk/{id}','BukuController@edit');

Route::get('dokumentasi', function (){
	return View('masterlayout.dokumentasi');
}); 
Route::get('peminjaman','PeminjamanController@index');
Route::get('tambahpinjam','PeminjamanController@create');
Route::post('tambahpinjam','PeminjamanController@store');
Route::post('/updatepinjam/{id}','PeminjamanController@update');
Route::get('/hapuspinjam/{id}','PeminjamanController@destroy');
Route::get('/editpinjam.{id}','PeminjamanController@edit');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

