<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuModel extends Model
{
    protected $table = 'tbbuku';
    public $primrykey = 'id';
    public $timestamps =true;
}
