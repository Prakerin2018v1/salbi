<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamanModel extends Model
{
    public $table = 'tbpeminjaman';
    public $primrykey = 'id';
    public $timestamps =true;
    public $fillable = [
    	'id',
    	'kd_buku',
    	// 'gambar',
    	'kategori',
    	'judul',
    	'penulis',
    	'penerbit',
    	'deskripsi',
    	'harga'
    ];
}
