<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GudangModel;

class GudangController extends Controller
{
     public function index()
    {
    $data = GudangModel::all();
    return view('penggudangan',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahgudang',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kd_buku'=>'required',
             'judul'=>'required',
             'penulis'=>'required',
             'penerbit'=>'required',
             'thn_terbit'=>'required',  
             'jumlah'=>'required',          
            'tgl_gudang'=>'required'
            ]);

        $GudangModel = new GudangModel;
        $GudangModel->kd_buku = $request->input('kd_buku'); 
        $GudangModel->judul = $request->input('judul');
        $GudangModel->penulis = $request->input('penulis');
         $GudangModel->penerbit = $request->input('penerbit');
         $GudangModel->thn_terbit = $request->input('thn_terbit');
         $GudangModel->jumlah = $request->input('jumlah');
         $GudangModel->tgl_gudang = $request->input('tgl_gudang');
        $GudangModel->save();
        return redirect ('/penggudangan')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $data = GudangModel::where('id',$id)->get();

        return view('editgudang',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'kd_buku'=>'required',
             'judul'=>'required',
             'penulis'=>'required',
             'penerbit'=>'required',
             'thn_terbit'=>'required',  
             'jumlah'=>'required',          
            'tgl_gudang'=>'required'
            ]);

        $data = array ('kd_buku' => $request->input('kd_buku'),
                        'judul' => $request->input('judul'),
                        'penulis' => $request->input('penulis'),
                        'penerbit' => $request->input('penerbit'),
                        'thn_terbit' => $request->input('thn_terbit'),
                        'jumlah' => $request->input('jumlah'),
                        'tgl_masuk' => $request->input('tgl_masuk'));
        GudangModel::where('id',$id)->update($data);
        return redirect ('/penggudangan')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
       GudangModel::where('id',$id)->delete();
        return redirect('/penggudangan')->with('info','Barang Berhasil Dihapus!');
}

}
