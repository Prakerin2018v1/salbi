<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PeminjamanModel;

class haldepController extends Controller
{
	public function index(){
	 $data = PeminjamanModel::where('kategori', 'Biografi')->get();
	 $novel = PeminjamanModel::where('kategori', 'Novel')->get();
	 $komik = PeminjamanModel::where('kategori', 'Komik')->get();
	 $dongeng = PeminjamanModel::where('kategori', 'Dongeng')->get();
	 $motivasi = PeminjamanModel::where('kategori', 'Motivasi')->get();
	 $tafsir = PeminjamanModel::where('kategori', 'Tafsir')->get();
	 return view('frontend.index',compact('data', 'novel', 'komik', 'dongeng', 'motivasi', 'tafsir' ));
   }
}
