<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BukuModel;

class BukuController extends Controller
{
     public function index()
    {
    $data = BukuModel::all();
    return view('pemasukan',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahmasuk',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required',            
            'ket'=>'required'
            ]);

        $BukuModel = new BukuModel;
        $BukuModel->nama = $request->input('nama'); 
        $BukuModel->ket = $request->input('ket');
        $BukuModel->save();
        return redirect ('/pemasukan')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $data = BukuModel::where('id',$id)->get();

        return view('editmasuk',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama'=>'required',
            'ket'=>'required'
            ]);

        $data = array ('nama' => $request->input('nama'),
                        'ket' => $request->input('ket'));
        BukuModel::where('id',$id)->update($data);
        return redirect ('/pemasukan')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
       BukuModel::where('id',$id)->delete();
        return redirect('/pemasukan')->with('info','Barang Berhasil Dihapus!');
}

}
