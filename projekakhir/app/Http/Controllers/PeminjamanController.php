<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeminjamanModel;
use App\Http\Request\PinjamCreateRequest;

class PeminjamanController extends Controller
{
     public function index()
    {
    $data = PeminjamanModel::all();
    return view('peminjaman',compact('data'));
    }
    public function create()
    {
        return view('tambahpinjam');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'kd_buku'=>'required',
        //     'gambar'=>'required|image',  
        //     'kategori'=>'required',
        //     'judul'=>'required',
        //     'penulis'=>'required',
        //     'penerbit'=>'required',  
        //     'deskripsi'=>'required',  
        //     'harga'=>'required'
        //     ]);



        $PeminjamanModel = new PeminjamanModel;
        $PeminjamanModel->kd_buku = $request->input('kd_buku'); 
        // $PeminjamanModel->gambar = $request->input('gambar');
        // $PeminjamanModel = $request->file('gambar');
        // $PeminjamanModel = $gambar->getClientOriginalName();
        // $PeminjamanModel->file('gambar')->move('uploadgambar', $PeminjamanModel);
        // $PeminjamanModel = new Peminjaman($request->all());
        // $PeminjamanModel->gambar = $PeminjamanModel;
        // $PeminjamanModel->save();
        $PeminjamanModel->kategori = $request->input('kategori');
        $PeminjamanModel->judul = $request->input('judul');
        $PeminjamanModel->penulis = $request->input('penulis');
        $PeminjamanModel->penerbit = $request->input('penerbit');
        $PeminjamanModel->deskripsi = $request->input('deskripsi');
        $PeminjamanModel->harga = $request->input('harga');
        $PeminjamanModel->save();
        return redirect ('peminjaman')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $data = PeminjamanModel::where('id',$id)->get();

        return view('editpinjam',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
             'kd_buku'=>'required',
             // 'gambar'=>'required', 
             'kategori'=>'required',
             'judul'=>'required',
            'penulis'=>'required',
             'penerbit'=>'required',  
             'deskripsi'=>'required',  
             'harga'=>'required'
             ]);

        $data = array ('kd_buku' => $request->input('kd_buku'), 
                        // 'gambar' => $request->input('gambar'),
                        'kategori' => $request->input('kategori'),
                        'judul' => $request->input('judul'),
                        'penulis' => $request->input('penulis'),
                        'penerbit' => $request->input('penerbit'),
                        'deskripsi' => $request->input('deskripsi'),
                        'harga' => $request->input('harga'));
        PeminjamanModel::where('id',$id)->update($data);
        return redirect ('/peminjaman')->with('info','Barang Berhasil di Simpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
       PeminjamanModel::where('id',$id)->delete();
        return redirect('/peminjaman')->with('info','Barang Berhasil Dihapus!');
}

}