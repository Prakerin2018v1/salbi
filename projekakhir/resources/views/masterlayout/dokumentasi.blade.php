@extends('frontend.index')
@section('content')


<div style="margin-top: 100px; background-color: rgb(255,255,255);">
            <div id="main">
<section class="p-0" id="portfolio">
     <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">DOKUMENTASI</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/4.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/4.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   photo
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/5.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/5.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/6.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/6.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/7.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/7.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/8.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/8.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/9.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/9.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/10.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/10.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                   photo
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/11.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/11.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/12.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/12.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/13.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/13.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/14.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/14.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/15.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/15.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    PHOTO
                  </div>
                  <div class="project-name">
                    KEGIATAN AL-ITTIHAD
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Mari Membaca</h2>
        <a class="btn btn-light btn-xl sr-button"  href="https://thisissalbiblogger.blogspot.com/">Buka Sekarang</a>
      </div>
    </section>

    @endsection