@extends('frontend.index')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  <br>
  <br>
<div class="container">
  <h2>Koleksi Biografi</h2>
  <p></p>
  <table class="table table-bordered ">
    <thead>
      <tr>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Biografi Ki Hajar Dewantara</td>
        <td>Biografi Cut Nyak Dien</td>
        <td>Biografi Habibie</td>
      </tr>      
      <tr class="success">
        <td>Biografi Habits</td>
        <td>Biografi Dan Brown</td>
        <td>Biografi Mochtar Lubis</td>
      </tr>
      <tr class="danger">
        <td>Biografi SYIBIL</td>
        <td>Biografi S.K.Trimurti</td>
        <td>Biografi 4 Serangkai Imam Madzhab</td>
      </tr>
      <tr class="info">
        <td>Biografi Ali Said</td>
        <td>Biografi Malcolm X</td>
        <td>Biografi Stephen King</td>
      </tr>
      <tr class="warning">
        <td>Biografi Soekarno</td>
        <td>Biografi Kiai Hamid</td>
        <td>Biografi Sidney Sheldon's</td>
      </tr>
      <tr class="active">
        <td>Biografi Gus Dur</td>
        <td>Biografi Sang Sufi</td>
        <td>Biografi Bill Gates</td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
@endsection