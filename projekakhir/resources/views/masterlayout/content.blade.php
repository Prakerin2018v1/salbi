@extends('frontend.index')
@section('content')
<header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-Tahoma">
              <strong>PustakaTren</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Membaca adalah nafas hidup dan jembatan emas ke masa depanku</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Tentang PustakaTren</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">PustakaTren</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">PustakaTren adalah sebuah perpuastakaan koleksi buku dan majalah. Walaupun dapat diartikan sebagai koleksi pribadi perseorangan, namun perpustakaan lebih umum dikenal sebagai sebuah koleksi besar yang dibiayai dan dioperasikan oleh sebuah kota atau institusi, serta dimanfaatkan oleh masyarakat yang rata-rata tidak mampu membeli sekian banyak buku atas biaya sendiri.</p>
          
          </div>
        </div>
      </div>
    </section>
<br>
<br>
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            @foreach($data as $datas)
            <h2 class="section-heading"> {{ $datas-> }} </h2>
             @endforeach
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <a href="novel">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fas fa-book text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Novel</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="biografi">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3" >Biografi</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="dongeng">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fas fa-book text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Dongeng</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="komik">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Komik</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/01.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/01.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                    Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/02.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/02.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/03.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/03.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/04.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/04.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/05.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/05.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/06.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/06.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Mari Membaca</h2>
        <a class="btn btn-light btn-xl sr-button"  href="https://thisissalbiblogger.blogspot.com/">Buka Sekarang</a>
      </div>
    </section>

    @endsection