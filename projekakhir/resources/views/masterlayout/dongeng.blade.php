@extends('frontend.index')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Koleksi Buku Dongeng</h2>
  <p></p>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Aku Kakak yang Baik</td>
        <td>Legenda Naga dari seluruh dunia</td>
        <td>Gajah Abrahah</td>
      </tr>      
      <tr class="success">
        <td>Burung dan Paus</td>
        <td>Qabil dan Nabil</td>
        <td>Sapi Bani Israil</td>
      </tr>
      <tr class="danger">
        <td>Unta Betina Nabi Shalih</td>
        <td>Anjing Ashabul Kahfi</td>
        <td>Burung Hud-Hud</td>
      </tr>
      <tr class="info">
        <td>Keledai Nabi Uzair</td>
        <td>Semut dan Nabi Sulaiman</td>
        <td>Badak Baik Hati</td>
      </tr>
      <tr class="warning">
        <td>Andai Cican jadi Polisi</td>
        <td>Si Kancil</td>
        <td>Serunya Pergi Ke Museum</td>
      </tr>
      <tr class="active">
        <td>Bajak Laut</td>
        <td>Tamasya Keliling Kota</td>
        <td>Waah! Cican Sakit Gigi</td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
@endsection