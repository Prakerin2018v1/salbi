@extends('frontend.index')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Koleksi Novel</h2>
  <p></p>
  <table class="table table-bordered ">
    <thead>
      <tr>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
        <th>Judul Buku</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Bulan - Tere Liye</td>
        <td>Musimah Sejati</td>
        <td>Surga yang Tak Di Rindukan</td>
      </tr>      
      <tr class="success">
        <td>Matahari - Tere Liye</td>
        <td>Sebuah Usaha Melupakan - Boy Candra</td>
        <td>I wuff You - Tere Liye</td>
      </tr>
      <tr class="danger">
        <td>Hujan - Tere Liye</td>
        <td>Dilan 1990 -  Boy Candra</td>
        <td>Cinta yang Rumit - Boy Candra</td>
      </tr>
      <tr class="info">
        <td>Bumi - Tere Liye</td>
        <td>Ayah - irfan Hamka</td>
        <td>Setelah Hujan Reda - Boy Candra</td>
      </tr>
      <tr class="warning">
        <td>Semoga Bunda Disayang Allah - Tere Liye</td>
        <td>This Is How I Do - Lia Indra</td>
        <td>Catatan Pendek - Boy Candra</td>
      </tr>
      <tr class="active">
        <td>Matt and Mou - Tere Liye</td>
        <td>Meteor - Tere Liye</td>
        <td>Ayat-ayat Cinta 2 - Drs.Habiburahma</td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
@endsection