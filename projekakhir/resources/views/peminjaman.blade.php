@extends('frontend.front')
@section('badan')
    <!-- Main Section -->
    <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
        <div class="content-wrapper">
        <div class="container-fluid">
        <ol class="breadcrumd">
    </ol>

         <div class="card mb-3">
        <div class="card-header">
        <div class="card-body">
          <div class="table-responsive">
        <div class="content">
            <!-- Remove This Before You Start -->
            <br>
            <br>
            <h1>Data Buku</h1>
            <a class="btn btn-md btn-primary" href="{{url('tambahpinjam')}}">Tambah Buku</a>
    <section class="main-section">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <!-- <th>Gambar</th>
                    <hr>
                    <div class="row">
                    <div class="col-md-6">
                    @if(count($data) > 0)
 
                        @foreach ($data as $file)
                        <img src="{{ url('uploadgambar') }}/{{ $file->file_gambar }}" class="img-responsive">
                        @endforeach
 
                    @endif
                    </div>
                    </div> -->
                    <th>Kategori</th>
                    <th>Judul</th>
                    <th>Penulis</th>
                    <th>Penerbit</th>
                    <th>Deskripsi</th>
                    <th>Harga</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @foreach($data as $datas)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $datas->kd_buku }}</td> 
                       <!--  <td>{{ $datas->gambar }}</td> -->
                        <td>{{ $datas->kategori }}</td>
                        <td>{{ $datas->judul }}</td>
                        <td>{{ $datas->penulis }}</td>
                        <td>{{ $datas->penerbit }}</td>
                        <td>{{ $datas->deskripsi }}</td>
                        <td>{{ $datas->harga }}</td>
                        <td>
                            <form action="" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                
                                  <a href='{{url("/hapuspinjam/{$datas->id}")}}' class="btn btn-danger btn-sm">Delete</a>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection