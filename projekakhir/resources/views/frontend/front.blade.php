<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.kepala')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.pundak')
            </header>
        
            <div id="main">
                @yield('badan')
            </div>

            <footer>
                @include('masterlayout.kaki')
           </footer>    
    </body>
</html>