<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
<header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-Tahoma">
              <strong>PustakaTren</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Membaca adalah nafas hidup dan jembatan emas ke masa depanku</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Tentang PustakaTren</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">PustakaTren</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">PustakaTren adalah sebuah perpuastakaan koleksi buku dan majalah. Walaupun dapat diartikan sebagai koleksi pribadi perseorangan, namun perpustakaan lebih umum dikenal sebagai sebuah koleksi besar yang dibiayai dan dioperasikan oleh sebuah kota atau institusi, serta dimanfaatkan oleh masyarakat yang rata-rata tidak mampu membeli sekian banyak buku atas biaya sendiri.</p>
          
          </div>
        </div>
      </div>
    </section>
<br>
<br>
<div class="row">

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <img  src="\img\kihajar-edit.jpg" class="card-img-top" alt="bulan"></a>
                <div class="card-body">
                   @foreach($data as $datas)
                <h3 class="mb-3">Biografi</h3>{{ $datas->judul }} </h2><br>
                                              {{ $datas->penulis }} </h2><br>
                                              {{ $datas->penerbit  }} </h2>
              @endforeach
                 
                  <p class="card-text"><!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur! --></p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="\img\Bulan-edit.jpg" alt=""></a>
                <div class="card-body">
                  @foreach($novel as $novels)
                <h3 class="mb-3">Novel</h3>{{ $novels->judul }} </h2><br>
                                            {{ $novels->penulis }} </h2><br>
                                            {{ $novels->penerbit  }} </h2>

              @endforeach
                 
                  <p class="card-text"><!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur! Lorem ipsum dolor sit amet. --></p>
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="\img\gagal.jpg" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                  @foreach($komik as $komiks)
                <h3 class="mb-3">Komik</h3>{{ $komiks->judul }} </h2><br>
                                           {{ $komiks->penulis }} </h2><br>
                                            {{ $komiks->penerbit  }} </h2>
              @endforeach
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="\img\negrijerman.jpg" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                   @foreach($dongeng as $dongengs)
                <h3 class="mb-3">Dongeng</h3>{{ $dongengs->judul }} </h2><br>
                                             {{ $dongengs->penulis }} </h2><br>
                                             {{ $dongengs->penerbit  }} </h2>
              @endforeach
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="\img\thing.jpg" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    @foreach($motivasi as $motivasis)
                <h3 class="mb-3">Motivasi</h3>{{ $motivasis->judul }} </h2><br>
                                              {{ $motivasis->penulis }} </h2><br>
                                              {{ $motivasis->penerbit  }} </h2>
              @endforeach
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="\img\tafsir.jpg" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                  @foreach($tafsir as $tafsirs)
                <h3 class="mb-3">Tafsir</h3>{{ $tafsirs->judul }} </h2><br>
                                            {{ $tafsirs->penulis }} </h2><br>
                                            {{ $tafsirs->penerbit  }} </h2>
              @endforeach
                </div>
                <div class="card-footer">
                  <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                </div>
              </div>
            </div>

    <!-- <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 text-center"> -->
            <br>
            <br>
            <hr class="my-2">
          </div>
        </div>
      </div>
     <!--  <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <a href="novel">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fas fa-book text-primary mb-3 sr-icons"></i>

               @foreach($data as $datas)        
                <h3 class="mb-3">Biografi</h3>{{ $datas->judul }} </h2>
              @endforeach
              
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="biografi">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
               @foreach($novel as $novels)
                <h3 class="mb-3">Novel</h3>{{ $novels->judul }} </h2>
              @endforeach
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="dongeng">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fas fa-book text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Dongeng</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <a href="komik">
            <div class="service-box mt-5 mx-auto">
              <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
              <h3 class="mb-3">Komik</h3>
              <p class="text-muted mb-0"></p>
            </div>
          </a>
          </div> -->
        </div>
      </div>
    </section>

    <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/01.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/01.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                    Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/02.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/02.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/03.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/03.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/04.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/04.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/05.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/05.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="img/portfolio/fullsize/06.jpg">
              <img class="img-fluid" src="img/portfolio/thumbnails/06.jpg" alt="">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Category
                  </div>
                  <div class="project-name">
                     Web Katalog
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Mari Membaca</h2>
        <a class="btn btn-light btn-xl sr-button"  href="https://thisissalbiblogger.blogspot.com/">Buka Sekarang</a>
      </div>
    </section>


            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>