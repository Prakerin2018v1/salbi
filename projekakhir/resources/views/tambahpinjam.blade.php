    @extends('frontend.front')
    @section('badan')
        <!-- Main Section -->
        <!-- Main Section -->
        <section class="main-section">
            <!-- Add Your Content Inside -->
            <div class="content">
            <div class="content-wrapper">
            <div class="container-fluid">
            <ol class="breadcrumd">
        </ol>

             <div class="card mb-3">
            <div class="card-header">
            <div class="card-body">
              <div class="table-responsive">
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1>Tambah Buku</h1>
                <hr>
                <form action="{{url('tambahpinjam')}}" method="post">
                    {{ csrf_field() }}
        
                    <div class="form-group">
                        <label for="kd_buku">Kode:</label>
                        <input type="text" class="form-control" id="kd_buku" name="kd_buku">
                    </div> 
                    <div class="form-group">
                        <label for="kategori">Kategori:</label>
                        <input type="text" class="form-control" id="kategori" name="kategori">
                    </div>
                    <div class="form-group">
                        <label for="judul">Judul:</label>
                        <input type="text" class="form-control" id="judul" name="judul">
                    </div>

                     <div class="form-group">
                        <label for="penulis">Penulis:</label>
                        <input type="text" class="form-control" id="penulis" name="penulis">
                    </div>

                     <div class="form-group">
                        <label for="id_siswa">Penerbit:</label>
                        <input type="text" class="form-control" id="penerbit" name="penerbit">
                    </div>

                     <div class="form-group">
                        <label for="deskripsi">Deskripsi:</label>
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi">
                    </div>
                   
                    <div class="form-group">
                        <label for="harga">Harga:</label>
                        <input type="text" class="form-control" id="harga" name="harga">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                        <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                    </div>
                </div>
            </div>
                </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    @endsection