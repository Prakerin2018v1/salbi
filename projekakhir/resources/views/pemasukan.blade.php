@extends('frontend.front')
@section('badan')
     <section class="main-section">
        <!-- Add Your Content Inside -->
        <div class="content">
        <div class="content-wrapper">
        <div class="container-fluid">
        <ol class="breadcrumd">
    </ol>

         <div class="card mb-3">
        <div class="card-header">
        <div class="card-body">
          <div class="table-responsive">
        <div class="content">
            <!-- Remove This Before You Start -->
            <br>
            <br>
            <h1>Data Kategori Buku</h1>
            <a class="btn btn-md btn-primary" href="{{url('/tambahmasuk')}}">Tambah Kategori Buku</a>
    <section class="main-section">
               
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 1; @endphp
                @foreach($data as $datas)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $datas->nama }}</td>
                        <td>{{ $datas->ket }}</td>
                        <td>
                            <form action="" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                
                             
                                 <!-- <a href='{{url("/editmasuk/{$datas->id}")}}' class="btn btn-info btn-sm">Edit</a> -->
                                  <a href='{{url("/hapusmasuk/{$datas->id}")}}' class="btn btn-danger btn-sm">Delete</a>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <!-- /.content -->
    </section>
    <!-- /.main-section -->
@endsection