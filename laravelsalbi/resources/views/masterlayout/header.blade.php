    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/">PondokKu</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="about">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="daftar">Pendaftaran</a>
            </li>
            <li class="nav-item">
             <div class="dropdown">
            <a class="dropdown-toggle nav-link text-uppercase text-white js-scroll-trigger" data-toggle="dropdown">Kegiatan</a>
            <div class="dropdown-menu">
           <a class="dropdown-item" href="ekstrau">Ekstrakulikuler umum</a>
           <a class="dropdown-item" href="ekstrap">Ekstrakulikuler pesantren</a>
           <a class="dropdown-item" href="ekstrao">Ekstrakulikuler Olahraga</a>
           </div>
           </div>
            </li>
            
           <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="galery">Gallery</a>
            </li>
            </ul>
        </div>
      </div>
    </nav>