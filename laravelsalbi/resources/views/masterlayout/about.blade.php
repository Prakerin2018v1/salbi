<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Sejarah AL-ITTIHAD</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">Di dekat Terminal Rawa Bango Desa Bojong Karangtengah Cianjur ±3 km dari kota Cianjur kearah Bandung terhampar ± 16.000 m2 tanah dari jalan raya kedalam. H. Acep Badrudin sebagai pemilik tanah tinggal di Jakarta Kp. Keramat Asem Kelurahan Utan Kayu Selatan Kec. Matraman, Jakarta Timur.

Beliau seorang pedagang yang sukses di Ciamis beristrikan (almh) Hj. Mimin Rukmini dari Tasik malaya. Allah memberikan rizki yang cukup walaupun tidak melimpah tapi berkah. Dari sebagian hasil usahanya di Jakarta di belikan tanah sawah di Cianjur secara bertahap dari mulai 600 m2 sejak tahun 1970 sampai 16.000 m2. Keberkahan rizkinya itu ditandai dengan niat yang ikhlas untuk mewakafkan tanah tersebut menjadi pesantren. Pada awalnya tanah ini sudah di hibahkan kepada enam anaknya : Neni Rahmani, Dewi Robiatul Adawiyah, Eti Muflihah, Kikim Kamilah, Ernawati Hifdiyah, dan Diah Mahdiah. Setelah di adakan musyawarah keluarga, akhirnya tanah seluas 13.000 m2 diwakafkan untuk pesantren dan yang sebanyak 3000 m2 dipinggir jalan raya dijadikan hak milik ke enam anaknya itu sebagai tempat usaha masing-masing.

Pada akhir tahun 1996 H. Acep Badrudin mengutarakan kepada anak-anaknya tentang keinginan mendirikan pesantren di Cianjur yang diharapkan oleh menantunya yang ke-3 (H.Kamali Abdul Ghani) dan anak cucunya yang mempunyai keahlian di bidang kepesantrenan.

Pada suatu ketika beliau berbicara serius dengan anakanya Hj. Eti Muflihah serta suaminya H.Kamali Abdul Ghani perihal bagaimana perencanaan pendirian Pondok Pesantren.H. Kamali Abdul Ghani putra ke-3 dari H. Abdul Ghani dan Hj. Ruqoyah berasal dari Kp. Beber Ds. Cigadung Kec.Banjarharjo Kab.Brebes.Setamat dari SD melanjutkan ke pesantren Lirboyo Kediri selama 13 tahun (tamat aliyah dan mengajar).Tahun 1983 diminta oleh K.H Sukron Ma’mun untuk mengajar kitab salafiyah dipesantren Darul Rahman Jakarta.Sambil mengajar juga kuliah di UMJ dan PTID Al-Akidah, tapi semuanya hanya sampai sarjana muda.Sampai-sampai al-wasilah yang hanya tinggal skripsi saja untuk mencapai gelar sarjana lengkap tidak terselesaikan.Rupanya Allah tidak menghendaki untuk menjadi seorang sarjana.Pada tahun 1990 menikah dengan alumni Pesantren Darul Rahman yang masih duduk di semester 4 UIN Jakarta bernama Eti Mufliah putri dari H. Acep Badruddin.

Niat baik H. Acep Badruddin untuk mendirikan Pesantren di cianjur di sambut positif oleh menantunya (K.H Kamali Abdul Ghani) namun waktu itu ia meminta untuk berfikir dan istikhoroh, karena ada pertimbangan ekonomis yang perlu di pikirkan. Bagaimana tidak terpikirkan walaupun bukan orang yang berhasil, tetapi untuk sehari-hari di Jakarta tidak terlalu riskan. Lalu bagaimana kalau di Cianjur ?dengan mendapatkan hidayah dari Allah akhirnya memutuskan niat baik mertuanya.

Pada tahun 1997 hijrahlah bersama istri dan ke-2 anaknya (Anisa Amelia dan Hasbi Rozak) ke Cianjur dengan bekal keyakinan bahwa Allah akan menolong kepada orang yang menolong agama-Nya. Rumah tua yang dibangun sekitar tahun 70-an serta mushola yang kurang terurus berukuran 5 x 7 M adalah tempat dimana beliau shalat sendiri atau berjama’ah dengan satu dua orang ma’mum. Dan juga beliau sempat berdagang material namun hanya bertahan 2 bulan saja. Setelah tinggal satu bulan di Cianjur mulailah pembangunan pesantren dengan modal uang tabungan almarhumah Hj. Mimin Rukmini, H. Acep Badrudin sangat senang sekali karena istrinya punya uang tabungan yang bisa dipakai untuk modal awal pembangunan pesantren.  Tahun pertama adalah lima lokal dengan standar sedang. Ketika hanya ngecor dok yang tiga lokal datangalah orang tua calon santri dari kampung dua bekasi untuk menitipkan anakanaknya di pesantren.Tetapi orang tua santri itu tetap percaya walaupun belum selesai, kata mereka nanti pada tahun ajaran baru pasti selesai.Hal inilah yang semakin menambah keyakinan untuk secepatnya untuk menyelesaikan pembangunan.Dengan izin Allah, sebagian pembanguan dapat terselesaikan pada tahun ajaran baru. Maka santri-santri dari bekasilah yang menjadi santri pertama di Pondok Pesantren Al-Ittihad, dan mereka adalah ; Faisal Karnain, Siti Usbah, Hikmah Handayani, Nana Supriatna dan Neneng Hasanah.

Kegiatan belajar baik Sekolah maupun Pesantren sudah dimulai pada tahun 1997-1998, walaupun hanya 14 santri yang berasal dari bekasi, Jakarta dan Cianjur. Dan baru pada tanggal 15 April Tahun 2000 M. diresmikanlah Pondok Pesantren Al-Ittihad oleh Bpk. KH.Syukron Ma’mun dengan membuka pendidikan formal SLTP dan SMU. Sejak itulah Pondok Pesantren Al-Ittihad mulai dikenal masyarakat, baik Cianjur maupun daerah lain. Terbukti pada Tahun Ajaran 2002 tercatat kurang lebih mencapai 450 santri dengan menggunakan kurikulum terpadu antara Pondok modern Gontor dan Pondok Salafiyah serta Pendidikan Nasional (SLTP dan SMU, Ponpes Al-Ittihad diharap mampu menjawab tantangan perkembangan Pengetahuan dan Teknologi, dan memenuhi kebutuhan masyarakat secara real. Adapun santri yang belajar di Pesantren ini berasal dari bergagai daerah diantaranya; Jawa Barat, Jawa Tengah, Jakarta dan luar Jawa.</p>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="didik">Pendidikan</a>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>