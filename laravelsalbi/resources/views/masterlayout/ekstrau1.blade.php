<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">KEGIATAN AL-ITTIHAD</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">
              
PRAMUKA AL-ITTIHAD<br><br>
1.  LATAR BELAKANG<br><br>
Gerakan pramuka adalah suatu gerakan yang di dalamnya terdiri atas anak-anak, pemuda, dan orang dewasa. Dilihat dari Psikologisnya, gerakan ini cocok diterapkan disekolah dan biasa dijadikan sebagai ekstrakulikuler, maka kami menganggap alangkah baiknya kalau diterapkan disekolah ini.
Dalam jaman modern ini bukan hanya otak yang ditingkatkan tapi juga skill sangat penting, maka dari itulah pramuka dipandang sangat menunjang proses pendidikan formal ini. Pramuka bukanlah suatu kegiatan yang dipandang sebelah mata karena didalam kepramukaan dituntut suatu wawasan yang luas.
Dalam hal ini kami melihat para siswa siswi di pondok pesantren Al-ittihad banyak sekali yang mengeluh, bosan, jenuh dan tak betah karena kegiatan yang monoton. Mudah-mudahan dengan adanya kegiatan kepramukaan ini mereka bisa terhibur dan mengurangu beban pikirannya yang selalu tertuju kerumah dikampung halaman. Kami bahagia saat melihat canda dan tawa. Hilang rasa lelah, letih dan penat kala membimbing mereka, hanya dengan senyuman manis yang berkembang di raut wajahnya.
Kami pun tak lepas dari pembinaan kakak Anton Musa dan Kakak Yusri Al-ittihad, karena merekalah kami merasakan kepuasan dan persaudaraan serta menanamkan rasa disiplin pada diri kami.<br><br>
2.  TUJUAN<br><br>
Gerakan Pramuka adalah gerakan yang mempunyai tujuan. Segala sesuatu  yang sehat pastilah memunyai tujuan dan Insya Allah tujuan tersebut bersifat postif adanya.<br>
Beberapa tujuan pramuka yaitu :<br>
• Memebtuk inssan yang bertakwa kepada Tuhan Yang Maha Esa.<br>
• Sebagai alat penghubur untuk anak-anak yang di dalamnya.<br>
• Salah satu alat untuk belajar berorganisasi.<br>
• Penambah wawasan.<br>
• Pembentuk Kepribadian.<br>
• Penumbuh kembangan jemampuan.<br>
• Wadah umtuk bertukar pikiran.<br>
• Alat silaturahim.<br>
• Membentuk manusia pancasila.<br><br>
Sebuah tujuan adalah Prioritas utama yang akan dipegang sebagai titik balik pada dirinya sendiri dan untuk mencapai tujuannya. Kependudukannya dari berbagai pihak, baik dari dalam maupun luar.<br><br><br>

         <a class="btn btn-light btn-xl js-scroll-trigger button_waterpump" href="ekstrau">PASKIBRA</a>
         <a class="btn btn-light btn-xl js-scroll-trigger button_airpump" href="ekstrau2">PMR</a>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>