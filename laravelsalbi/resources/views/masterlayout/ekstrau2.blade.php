<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
 <section class="bg-primary" id="about">

      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">KEGIATAN AL-ITTIHAD</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">
              
            PMR AL-ITTIHAD<br><br><br>
         <a class="btn btn-light btn-xl js-scroll-trigger button_waterpump" href="ekstrau1">PRAMUKA</a>
         <a class="btn btn-light btn-xl js-scroll-trigger button_airpump" href="ekstrau">PASKIBRA</a>
          </div>
        </div>
      </div>
    </section>

            </div>
    </body>
</html>