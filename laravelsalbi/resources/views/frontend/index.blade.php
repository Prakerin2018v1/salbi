<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
                @yield('content')
            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>